#!/usr/bin/bash
#
#  make_rootfs
#
# Created by Danny Goossen on 28/12/2017.
#
# MIT License
#
# Copyright (c) 2018 odagrun, Gioxa Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
. trap_print
f_quiet_command
echo -e "\033[36;1m ++++++++++++++++++++++++++++++++++++\n"
echo -e "    Create rootfs from rpm distro\n"
echo -e " ++++++++++++++++++++++++++++++++++++\n\033[0;m"

if [ -z "$BASE" ]; then
  BASE=$PWD
fi
if [ -z "$target" ]; then
  target=$PWD/rootfs
fi
export BASE
export target

mkdir -pv "$target"
mkdir -pv "${BASE}/cache"

if [ ! -d "/${target}" ]; then
  echo -e "\033[35m\n target dir: \'$target\' not found!!!\n\n!!! define \'\$target\' as an absolute path!!!\n\033[0;m"
  /bin/false
fi
rm -fr $target/*

if [ ! -f "$OS_CONFIG" ]; then
  echo -e "\033[35m\n Define OS_CONFIG variable pointing to an existing \'make_os.conf\' in current dir, abort!!\033[0;m\n"
  f_fail
fi
. $OS_CONFIG
if [ -z "$basearch" ]; then
  basearch=x86_64
  echo -e "\033[33m Using make_os default basearch=x86_64.\033[0;m\n"
fi

if [ -z "$yum_config" ]; then
  yum_config=yum.conf
fi
if [ ! -f "$yum_config" ]; then
  cat > "$yum_config" <<'EOF'
[main]
cachedir=/var/cache/yum/$basearch/$releasever
keepcache=1
debuglevel=1
logfile=/yum.log
exactarch=1
obsoletes=1
gpgcheck=1
plugins=1
installonly_limit=5
distroverpkg=centos-release
reposdir=./yum.repos.d/
metadata_expire=90m
http_caching=all
color=off
EOF
  echo -e "\033[33m Using make_os default yum_config.\033[0;m\n"
fi

if [ -d "/usr/sbin/glibc-fake" ]; then
  GLIBC_FAKE=/usr/sbin/glibc-fake
else
   echo -e "\033[35m /usr/sbin/glibc-fake NOT FOUND \n\n!!! use \'gioxa/imagebuilder\' docker image for build!!!\n\033[0;m\n\n"
   f_fail
fi
export GLIBC_FAKE

  # need to use host sln
  SLND="/usr/sbin/sln"
  
f_verbose_command
if [ -z "$DISTRO_RELEASE" ]; then
    echo -e "\033[33m Using make_os default DISTRO_RELEASE: 7\033[0;m\n"
    DISTRO_RELEASE=7
fi
echo
echo " Target relase : $DISTRO_RELEASE"

echo -e "\n\033[36m-------- RPM prepare user db ------\033[0;m"
mkdir -pv $target/var/lib/rpm
rpmdb --initdb --dbpath $target/var/lib/rpm

f_quiet_command
echo "%_dbpath $target/var/lib/rpm" | cat > $HOME/.rpmmacros
echo "%_tmppath /tmp" | cat >> $HOME/.rpmmacros
f_verbose_command

echo -e "\n\033[36m-------- RPM import pub keys ------\033[0;m"

rpm --verbose --import $BASE/GPG-KEYS/*

rpm -q gpg-pubkey --qf '%{NAME}-%{VERSION}-%{RELEASE}\t%{SUMMARY}\n'

# TODO figure out how to do different archs
#
echo -e "\n\033[36m-------- prepare yum cache ------\033[0;m"


rm -rf "$target/var/cache/yum"
mkdir -pv "$target/var/cache"
mkdir -pv $BASE/cache/yum
# rm -fv $BASE/cache/yum/*/*/*/gen/*

# overlay trouble fix, don't touch the pakages, increase usage of mem in openshift online
find $BASE/cache/yum/*/*/*/{*.xml,*.bz2,gen/*} -type f -exec touch {} + || /bin/true

rm -f $BASE/cache/yum/*/*/*/gen/*

ln -svf "$BASE/cache/yum" "$target/var/cache/"

echo -e "\n\033[36m-------- YUM install packages ------\033[0;m"
f_quiet_command
# fool rpm without prefix for db (rpm bug)
echo "%_dbpath var/lib/rpm" | cat > $HOME/.rpmmacros
echo "%_tmppath /tmp" | cat >> $HOME/.rpmmacros
echo "%__transaction_systemd_inhibit %{nil}" | cat >> $HOME/.rpmmacros

# Fakechroot exclude paths
#
  FAKECHROOT_EXCLUDE_PATH="/tmp:/proc:$target:$BASE/packages:$BASE:/usr/sbin/build-locale-archive.rootfs:$SLND:$GLIBC_FAKE"
# Fakechroot command substitutions
#
  FAKECHROOT_CMD_SUBST=":/usr/sbin/glibc_post_upgrade.x86_64=$GLIBC_FAKE/glibc.post.upgrade.x86_64"
  FAKECHROOT_CMD_SUBST+=":/usr/sbin/build-locale-archive=$GLIBC_FAKE/build.locale.archive.wrap"
  FAKECHROOT_CMD_SUBST+=":/sbin/ldconfig=$GLIBC_FAKE/ld.config.wrap"
  FAKECHROOT_CMD_SUBST+=":/usr/bin/env=/usr/bin/env.fakechroot:/usr/bin/mkfifo=/bin/true"
  FAKECHROOT_CMD_SUBST+=":/usr/bin/ischroot=/bin/true:/sbin/insserv=/bin/true:/bin/mount=/bin/true"
  FAKECHROOT_CMD_SUBST+=":/usr/sbin/install-info=/bin/true"
  FAKECHROOT_CMD_SUBST+=":/usr/sbin/sln:$SLND:/sbin/sln=$SLND:sln=$SLND"
  FAKECHROOT_CMD_SUBST+=":/usr/bin/ca-legacy=$GLIBC_FAKE/ca-legacy-bi:/bin/ca-legacy=$GLIBC_FAKE/ca-legacy-bi:ca-legacy=$GLIBC_FAKE/ca-legacy-bi"

  export FAKECHROOT_EXCLUDE_PATH
  export FAKECHROOT_CMD_SUBST

  [ -n "$install_packages" ] && INSTALL_PACKAGES="install ${install_packages}"

  export INSTALL_PACKAGES
  
  f_verbose_command

  /usr/bin/fakechroot --use-system-libs -- /usr/bin/fakeroot -- /usr/bin/yum -c "$yum_config" --installroot="$target" --releasever="$DISTRO_RELEASE" -y $INSTALL_PACKAGES "${install_groups}"

  unlink "$target/var/cache/yum"
  rm -f $BASE/cache/yum/*/*/*/gen/*

  mkdir $target/var/cache/yum
  chmod 775 $target/var/cache/yum

  echo -e "\n\033[36m-------- /tmp and /root passwd ------\033[0;m"
  mkdir -pv $target/tmp
  chmod -v 777 $target/tmp
  if [ -d "$target/etc/passwd" ] ; then
    chmod -v 775  $target/etc/passwd
  fi

  if [ -d "$target/root" ] ; then   
    if [ -d "$target/etc/skel" ] ; then
      cp -fv /etc/skel/.bash* $target/root
    fi
  fi

  echo -e "\n\033[36m-------- cleanup ------\033[0;m"
  # cleanup
  #  locales
  if [ -z "$OS_KEEP_LOCALE" ]; then
    rm -rf "$target"/usr/{{lib,share}/locale,{lib,lib64}/gconv,bin/localedef,sbin/build-locale-archive}
  fi
  
  #  docs and man pages
  if [ -z "$OS_KEEP_MAN_and_DOCS" ]; then
    rm -rf "$target"/usr/share/{man/*,doc/*,info/*,gnome/help/*}
  fi 

  #  cracklib
  if [ -z "$OS_KEEP_CRACKLIB" ]; then
    rm -rf "$target"/usr/share/cracklib
  fi
  
  #  i18n
  if [ -z "$OS_KEEP_i18n" ]; then
    rm -rf "$target"/usr/share/i18n
  fi

  rm -f $HOME/.rpmmacros

  echo -e "\n\033[36m-------- RPM list packages ------\033[0;m"
  f_quiet_command
  echo "%_dbpath $target/var/lib/rpm" | cat > $HOME/.rpmmacros
  echo "%_tmppath /tmp" | cat >> $HOME/.rpmmacros
  f_verbose_command
  rpm -qa > $target/etc/system_packages
  echo "Installed $(cat  $target/etc/system_packages | wc -w | tr -d '\n') packages. listed at /etc/system_packages"

  echo -e "\n\033[36m-------- rewrite absolut symlinks outside $target ------\033[0;m"

  f_quiet_command
  cd $target
  echo -e "Rewriting symlinks to:\n"
  find . -lname '/*' | while read l ; do
      dest=$(readlink $l);
      if [ -d "$dest" ]; then
         unlink $l;
      fi
      echo "ln -sfv $(echo "$dest" | sed 's|'$target'/|/|') $l";
  done | bash
  cd $BASE
  f_verbose_command
    
    if [ -n "$postscript" ]; then
      echo -e "\n\033[36m-------- postcript ------\033[0;m"
      . $postscript
    fi

trap - DEBUG
