
## purpose

Image for building images from a CentOS RPM repository as non-root with fake(ch)root from GitLab-CI on openshift with odagrun.

## Details

The `imagebuilder` is build in 2 stages, starting from an official CentOS7 from the dockerhub to create an image to build the fake_xxx rpms in pass two and push it to the registry.

The `imagebuilder` is kept small and contains besides the linux coreutils, yum, rpm : 

- fakechroot
- fakeroot
- `libfakearch.so`, an **LD_PRELOAD** lib to fake a different architecture by setting the envirnoment variable **FAKE_ARCH**.
- `trap_print`, a script helper to catch errors and a `set +x` alternative for printing variable expanded command.
- make_os, a script to build a rootfs from scratch with centos rpm repository.(see futher)

In order to be able to create a **rootfs** with `yum --installroot=xxx` as non-root a few wrappers and stubs are provided in `/usr/sbin/glibc-fake`   :

- ldconfig wrapper, if env. var `FAKE_SKIP_LDCONFIG` is set ldconfig wrapper will return true
- glibc-post upgrade
- ca-legacy-bi
- build-locale-archive.wrap, if `SKIP_BUILD_LOCALE_ARCHIVE` is set /sbin/build-locale-archive will be skipped

## make_os

### Purpose

make_os is a script that set's the environment for fakeroot,fakechroot and a few wrappers/stubs to build a rootfs system with `yum --installroot=xxx` for a given `make_os.config`.

### prereqisites:

- define the `OS_CONFIG` variable pointing to a `make_os.conf` and create that `make_os.conf`.

Sample `make_os.conf` to create a minimal `rootfs` with CentOS 7 coreutils:

```bash
install_packages="coreutils"

#
# uncomment to use a group install:
#install_groups="@Development Tools"
#

#
# uncomment to add a post script, use absolute path, 
# BASE points to the root of the project_dir
#postscript=$BASE/postscript
#

#
# uncomment if one would like to preserve some by default cleaned sections:
#OS_KEEP_CRACKLIB=True
#OS_KEEP_LOCALE=True
#OS_KEEP_MAN_and_DOCS=True
#OS_KEEP_i18n=True

#
# uncomment to change the base arch, for future extensions
#basearch=x86_64
#

#
# To define a diferent yum_conf, uncomment and edit
#yum_config=yum.conf
#

#
# uncomment and set desired DISTRO_RELEASE
#DISTRO_RELEASE=7
#

```

- Create directory `yum.repo.d` and add the repo's for  yum to use
- Create Directory `GPG-KEYS`, all keys in this deirectory will be imported.

- if the default `yum.conf` does not exists if will be create as:

```
[main]
cachedir=/var/cache/yum/$basearch/$releasever
keepcache=1
debuglevel=1
logfile=/yum.log
exactarch=1
obsoletes=1
gpgcheck=1
plugins=1
installonly_limit=5
distroverpkg=centos-release
reposdir=./yum.repos.d/
metadata_expire=90m
http_caching=all
color=off
```

- define optional a different directory for the new rootfs to create with:

```
export target=rootfs
``` 

- call make_os:

### Create rootfs:

```
make_os
```

and the directory as pointed to with target, default ./rootfs will contain the new docker os image layer.

### cavecats

- no support yet for different archtectures
- all files in rootfs are owned by the current user, use odagrun registry_push command and they'll be owned by `root:root`
- no support for service users

### yum caching

make_os will link the directory `$BASE/cache/yum` into the new rootfs and remove the cache form the newly create rootfs, with base pointing to the `CI_PROJECT_DIR`.

To create a cache image layer with odagrun gitlab-runner define a variable in gitlab-ci.yml f.i.:

```
variables:
    WORK_SPACES: |
          - name: "repocache C${DISTRO_RELEASE}"
            key: x86_64
            scope: global
            path:
             - cache/yum/x86_64/${DISTRO_RELEASE}/base
             - cache/yum/x86_64/${DISTRO_RELEASE}/updates
            strategy: push-pull
            mandatory: false
```

This to reduce the bandwithd usage to the public centos(vault).

### installed packages:

are listed in `$target/etc/system_packages`, obtained with `rpm -qa > $target/etc/system_packages`

### special modes:

- /ect/passwd mode is changed, to allow adding the non root user on start, with:

```
chmod 775  $target/etc/passwd
```

- /tmp is created with 777


### examples projects build with this `imagebuilder`-image

 - [buildimage-odagrun](https://gitlab.com/gioxa/odagrun/buildimage)
 - [webbuildimage odagrun](https://gitlab.com/gioxa/odagrun/webbuildimage)
 - [build-rpmbuild-ruby](https://gitlab.com/gioxa/build-images/build-rpmbuild-ruby)
 - [fpm-centos](https://gitlab.com/gioxa/build-images/fpm-image)

 or checkout [gioxa/buildimages](https://gitlab.com/gioxa/build-images)

