# ${ODAGRUN_IMAGE_REFNAME}


[![](https://images.microbadger.com/badges/version/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/image/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/license/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME} "Get your own license badge on microbadger.com") [![](https://img.shields.io/badge/Gitlab-source-blue.svg)](${CI_PROJECT_URL})

${README_MD}


## Image Docker Config

```yaml
$DOCKER_CONFIG_YML
```

## example usage 

For use with gitlab-ci and odagrun 

- create a new GitLab project
- add a runner (odagrun), see [install odagrun](https://www.odagrun.com/docs/setup-odagrun-okd/)
- create a `.gitlab-ci.yml` file:

```yaml
build:
  image: ${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}:latest
  script:
  - export OS_CONFIG=make_os.conf
  - make_os
  - registry_push --rootfs --ISR --reference=$${CI_PIPELINE_ID}
  tags: odagrun
```
- create `make_os.conf` file and content:

```
# make_os.conf
install_packages="coreutils"
```
- create `yum.repos.d` with a e.g. : `base.repo` file.

```
# base.repo
[base]
name=CentOS-$releasever - Base
mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=os&infra=$infra

[updates]
name=CentOS-$releasever - Updates
mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=updates&infra=$infra

```
- create GPG-KEYS directory with a file : [RPM-GPG-KEY-CentOS-7](https://www.centos.org/keys/RPM-GPG-KEY-CentOS-7)

- commit:

*Result:* a CentOS 7 nano docker image with *coreutils* pushed to `ImageStream:$${CI_PIPELINE_ID}`

---

*Build with [odagrun](https://www.odagrun.com) on openshift-online-starter*

